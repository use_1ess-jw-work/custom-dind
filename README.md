# custom-dind

Little repo with basic docker image with docker daemon enabled internally

To build
>`docker build -t my_debian:dind -f Dockerfile_dind .`

To run
>`docker run -it --rm --name test_dind --privileged -v $(pwd)/Dockerfile_for_dind:/my_dir/for_docker/Dockerfile  my_debian:dind`

To start internal docker daemon
>`service docker start`

To build intenal image
>`docker build -t internal_dind_image:0.1 .`

To run internal image
>`docker run -it --rm internal_dind_image:0.1`

